<?php

/**
 * Class BookWidget
 */
class BookWidget extends WP_Widget {

    /**
     * BookWidget constructor.
     */
    public function __construct() {
        parent::__construct(
            'wtms_book_widget',
            __( 'Recent Books', 'text_domain' ),
            array(
                'customize_selective_refresh' => true,
            )
        );
    }

    /**
     * Display widget form
     *
     * @param $instance
     */
    public function form($instance) {
        $defaults = array(
            'noofbooks'=> 2
        );
        extract(wp_parse_args((array)$instance, $defaults));
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id('noofbooks')); ?>"><?php _e('Number of books', 'text_domain'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('noofbooks')); ?>" name="<?php echo esc_attr($this->get_field_name('noofbooks')); ?>" type="number" value="<?php echo esc_attr($noofbooks); ?>" />
        </p>
        <?php 
    }

    /**
     * Update widget settings
     *
     * @param $new_instance
     * @param $old_instance
     * @return mixed
     */
    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['noofbooks'] = isset($new_instance['noofbooks']) ? wp_strip_all_tags($new_instance['noofbooks']) : 2;
        return $instance;
    }

    /**
     * Display the widget
     *
     * @param $args
     * @param $instance
     */
    public function widget($args, $instance) {
        extract( $args );
        $noofbooks  = isset($instance['noofbooks']) ? $instance['noofbooks'] : 2;
        echo $before_widget;
        echo '<div class="widget-text wp_widget_plugin_box">';
        echo $before_title . __('Recent Books', 'text_domain') . $after_title;                    
        $bookargs = array( 
          'numberposts'        => $noofbooks,
          'post_type'        => 'book'
        );
        $books = get_posts($bookargs);
        echo '<ul class="wpms">';
        foreach ($books as $book) {
            echo '<li>';
            if (get_the_post_thumbnail_url($book->ID)) {
                echo '<img src="' . get_the_post_thumbnail_url($book->ID) . '" />';
            }
            echo '<a href="' . get_post_permalink($book->ID) . '">' . get_the_title($book->ID) . '</a><br/><em>' . wp_trim_words($book->post_content, 8, '...') . '</em></li>';        
        }
        echo '</ul>';

        echo '</div>';
        echo $after_widget;
    }
}
