<?php

/**
 * Class BookPost
 */
class BookPost {

    /**
     * BookPost constructor.
     */
    public function __construct() {
        add_action('wp_enqueue_scripts', array($this, 'scripts'));
        add_action('init', array($this, 'create'));
        add_action('widgets_init', array($this, 'widget'));
    }


    /**
     * Create book (custom post type)
     */
    function create() {
        $labels = array(
            'name' => __( 'Books' ),
            'singular_name' => __( 'Book' ),
            'add_new' => __('Add book'),
            'all_items' => __('All books')
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'has_archive' => true,
            'publicly_queryable' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => true,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
                'custom-fields',
                'revisions'
            ),
            'taxonomies' => array( 'category', 'post_tag' ), 
            'menu_position' => 5,
            'exclude_from_search' => false
        );
        register_post_type('book', $args);

    }

    /**
     * Register widget
     */
    public function widget() {
        register_widget('BookWidget');
    }

    /**
     * Styling
     */
    public function scripts() {
        wp_register_style('wpms', plugins_url('/../css/style.css', __FILE__), array(), '1', 'all');  
        wp_enqueue_style('wpms');
    }

}
