<?php
/*
Plugin Name: Bookposts - LITK: WordPress Test 1
Description: WordPress Test 1 with custom book post types.
Author: Mikael Strömgren
Version: 1.0
*/

if (!defined('WPINC')) {
    die;
}

require_once(__DIR__ . '/class/BookPost.php');
require_once(__DIR__ . '/class/BookWidget.php');

$bp = new BookPost();
